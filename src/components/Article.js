import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import firstImage from "../images/first.jpg";
import secondImage from "../images/second.png";
import thirdImage from "../images/third.png";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { Wave, Random } from "react-animated-text";

const useStyles = makeStyles(theme => ({
  root: {
    background: "#409fff",
    paddingTop: 50
  },
  menuList: {
    display: "flex",
    float: "right"
  },
  card: {
    width: 280,
    height: 410,
    margin: 60
  },
  media: {
    height: 140
  },

  leftButton: {
    background: "transparent",
    borderColor: "#409fff",
    color: "#409fff",
    left: "50%",
    top: "40px",
    transform: "translate(-50%, -50%)",
    border: "1px solid #409fff",
    borderRadius: "1.25em"
  },
  rightButton: {
    left: "50%",
    transform: "translate(-50%, -50%)",
    border: "1px solid black",
    borderRadius: "1.25em"
  },
  boxContainer: {
    padding: 20
  },
  article: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    flexWrap: "wrap"
  },
  rightSide: {
    background: "none",
    boxShadow: "none"
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function Article() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <div className={classes.article}>
        <Card className={classes.card}>
          <CardActionArea className={classes.boxContainer}>
            <CardMedia
              className={classes.media}
              image={firstImage}
              title="image"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Transfer deleted
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Sorry, this transfer has been deleted and is not available any
                more
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button className={classes.leftButton}>Send a file?</Button>
          </CardActions>
        </Card>
        <Card className={classes.rightSide}>
          <CardActionArea className={classes.boxContainer}>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Sun, Circles and other shapes
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Sorry, this transfer has been deleted and is not available any
                more
              </Typography>
            </CardContent>
            <CardMedia
              className={classes.media}
              image={secondImage}
              title="image"
            />
            <CardMedia
              className={classes.media}
              image={thirdImage}
              title="image"
            />
          </CardActionArea>
          <CardActions>
            <Button className={classes.rightButton} onClick={handleClickOpen}>
              Read now
            </Button>
          </CardActions>
        </Card>
      </div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>

            <Button color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
      </Dialog>
      <Wave
        text="lorem ip A nice collection of often useful examples done in React.js
        React.js Examples Ui Material design List Cards Infinite Scroll
        Bootstrap Table Layout Scroll Single Page Responsive Style All UI Media
        Slides Slider Chart Lightbox Video Gallery Carousel Images Player Audio
        Music Movies Maps Elements Calculator Drag Tooltip Notifications
        Pagination Toggle Icons Emoji Progress Loading Svg Accordion Modals
        Popup Dialogs Color Clock Avatar Circular Sticky Input Calendar Form"
        effect="stretch"
        effectChange={2.0}
      />
    </div>
  );
}
