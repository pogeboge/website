import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

const useStyles = makeStyles(theme => ({
  root: {
    background: "#409fff"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },

  header: {
    display: "flex",
    justifyContent: "space-between"
  },
  menuList: {
    display: "flex",
    float: "right"
  },

  appBar: {
    background: "none",
    boxShadow: "none",
    position: "fixed"
  }
}));

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.header}>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <MenuList className={classes.menuList}>
            <MenuItem>Help</MenuItem>
            <MenuItem>About</MenuItem>
            <MenuItem>Sign in</MenuItem>
            <MenuItem>Upgrade</MenuItem>
          </MenuList>
        </Toolbar>
      </AppBar>
    </div>
  );
}
